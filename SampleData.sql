﻿Create Database Test3
go

Use  Test3
go

CREATE TABLE Reader(
PIN smallint PRIMARY KEY,
LName nvarchar(15) NOT NULL,
MName nvarchar(1),
FName nvarchar(15) NOT NULL,
DoB date,
)

CREATE TABLE AdultReader(
ReaderPIN smallint PRIMARY KEY FOREIGN KEY REFERENCES  Reader(PIN),
HouseNumber nvarchar(15) NOT NULL,
Street nvarchar(63) NOT NULL,
District nvarchar(2) NOT NULL,
Phone nvarchar(13) ,
Expired date NOT NULL,
)







CREATE TABLE ChildrenReader(
ReaderPIN smallint PRIMARY KEY FOREIGN KEY REFERENCES Reader(PIN),
GuaranteeReader smallint NOT NULL FOREIGN KEY REFERENCES AdultReader(ReaderPIN),
)


CREATE TABLE BookCopy(
ISBN int,
CopyNum smallint,
CStatus nvarchar(1),
PRIMARY KEY (ISBN, CopyNum)
)

CREATE TABLE BorrowedHistory(
ISBN int,
BookCopyNum smallint,
BorrowDate smalldatetime,
ReaderPin smallint NOT NULL,
MustReturnDate smalldatetime,
ReturnDate smalldatetime,
LateFine money,
Note nvarchar(255),
Primary key(ISBN,BookCopyNum,BorrowDate)
)

CREATE TABLE Borrowing(
ISBN int,
BCopyNum smallint,
ReaderPIN smallint,
BorrowDate smalldatetime,
MustReturnDate smalldatetime,
PRIMARY KEY (ISBN, BCopyNum)
)



CREATE TABLE Booking(
ISBN int,
ReaderPIN smallint,
BookingDate smalldatetime,
Note nvarchar(255),
PRIMARY KEY (ISBN, ReaderPIN),
)



CREATE TABLE BookEdition(
ISBN int PRIMARY KEY,
TitleID int,
ELanguage nvarchar(15),
Cover nvarchar(15),
EStatus nvarchar(1),

)

CREATE TABLE BookTitle(
id int PRIMARY KEY,
TitleName nvarchar(63),
Author nvarchar(31),
Summary nvarchar(222)
)


ALTER TABLE Borrowing
ADD CONSTRAINT fk_Borrowing_isbn_CopyNum
FOREIGN KEY (ISBN,BCopyNum)
REFERENCES BookCopy(ISBN,CopyNum)

ALTER TABLE Borrowing
ADD CONSTRAINT fk_Borrowing_ReaderPIN
FOREIGN KEY (ReaderPIN)
REFERENCES Reader(PIN)

ALTER TABLE Booking
ADD CONSTRAINT fk_Booking_ISBN
FOREIGN KEY (ISBN)
REFERENCES BookEdition(ISBN)

ALTER TABLE Booking
ADD CONSTRAINT fk_Booking_ReaderPIN
FOREIGN KEY (ReaderPIN)
REFERENCES Reader(PIN)

ALTER TABLE BookCopy
ADD CONSTRAINT fk_BookCopy_ISBN
FOREIGN KEY (ISBN)
REFERENCES BookEdition(ISBN)


ALTER TABLE BookEdition
ADD CONSTRAINT fk_BookEdition_isbn
FOREIGN KEY (TitleID)
REFERENCES BookTitle(id)

ALTER TABLE BorrowedHistory
ADD CONSTRAINT fk_BorrowedHistory
FOREIGN KEY (ISBN,BookCopyNum)
REFERENCES BookCopy(ISBN,CopyNum)

ALTER TABLE BorrowedHistory
ADD CONSTRAINT fk_BorrowedHistory_ReaderPin
FOREIGN KEY (ReaderPIN)
REFERENCES Reader(PIN)

-----------Insert-----------------------

------Reader------------
INSERT INTO Reader([PIN],[LName],[MName],[FName],[DoB])
VALUES (65,'Dương','L','Hà','1981-08-30')


INSERT INTO Reader([PIN],[LName],[MName],[FName],[DoB])
VALUES (105,N'Mạc','T','Lan','1981-09-01')
INSERT INTO Reader([PIN],[LName],[MName],[FName],[DoB])
VALUES (23,N'Đặng','P',N'Duyên','1990-06-09')
INSERT INTO Reader([PIN],[LName],[MName],[FName],[DoB])
VALUES (98,N'Bùi','H','Sang','2004-02-29')

INSERT INTO Reader([PIN],[LName],[MName],[FName],[DoB])
VALUES (96,N'Vũ','T','Anh','2005-06-29')
INSERT INTO Reader([PIN],[LName],[MName],[FName],[DoB])
VALUES (66,N'Lê','T','Khang','2004-09-09')
INSERT INTO Reader([PIN],[LName],[MName],[FName],[DoB])
VALUES (980,N'Bùi','H','Xang','1994-02-25')
INSERT INTO Reader([PIN],[LName],[MName],[FName],[DoB])
VALUES (960,N'Vũ','T','Canh','1995-06-29')
INSERT INTO Reader([PIN],[LName],[MName],[FName],[DoB])
VALUES (660,N'Lê','T','Khanh','1994-09-09')
-----------
insert Reader (PIN, LName, MName, FName, DoB)
values (31, N'Phạm', N'L', N'Nam', '1985-06-09')
insert Reader (PIN, LName, MName, FName, DoB)
values (12, N'Đỗ', N'B', N'Phương', '2006-07-15')
insert Reader (PIN, LName, MName, FName, DoB)
values (88, N'Lê', N'H', N'Linh', '2003-12-30')
insert Reader (PIN, LName, MName, FName, DoB)
values (7, N'Lê', N'B', N'Mộng', '1990-03-06')
insert Reader (PIN, LName, MName, FName, DoB)
values (113, N'Thằng', N'M', N'Mượn tất sách', '1990-03-06')
insert Reader (PIN, LName, MName, FName, DoB)
values (114, N'Thằng', N'M', N'Cuồng thái hậu', '1990-03-06')
INSERT INTO Reader([PIN],[LName],[MName],[FName],[DoB])
VALUES (99,N'Lê','V','Sang','2004-02-29')
----Adult Reader-----
INSERT INTO AdultReader([ReaderPIN],[HouseNumber],[Street],[District],[Phone],[Expired])
VALUES(65,'70',N'Bùi Thị Xuân','GV',NULL,'2015-09-20')
INSERT INTO AdultReader([ReaderPIN],[HouseNumber],[Street],[District],[Phone],[Expired])
VALUES(105,'50 bit',N'Lê Lợi','10',NULL,'2015-06-28')
INSERT INTO AdultReader([ReaderPIN],[HouseNumber],[Street],[District],[Phone],[Expired])
VALUES(980,'3/',N'Nguyễn Văn Cừ','6',NULL,'2015-05-20')
INSERT INTO AdultReader([ReaderPIN],[HouseNumber],[Street],[District],[Phone],[Expired])
VALUES(113,'3/',N'Nhà sách','6',NULL,'2015-05-20')
INSERT INTO AdultReader([ReaderPIN],[HouseNumber],[Street],[District],[Phone],[Expired])
VALUES(114,'3/',N'Nội cung','6',NULL,'2015-05-20')
INSERT INTO AdultReader([ReaderPIN],[HouseNumber],[Street],[District],[Phone],[Expired])
VALUES(99,'3/',N'asdsd','6',NULL,'2015-05-20')
-------
insert AdultReader (ReaderPIN, HouseNumber, Street, District, Phone, Expired)
values(23, N'10', N'Trần Hưng Đạo', N'2', NULL, '2015-08-23')
insert AdultReader (ReaderPIN, HouseNumber, Street, District, Phone, Expired)
values(960, N'20', N'Trần Phú', N'4', NULL, '2015-08-25')
insert AdultReader (ReaderPIN, HouseNumber, Street, District, Phone, Expired)
values(660, N'60', N'Nguyễn Trãi', N'BT', NULL, '2015-08-17')
insert AdultReader (ReaderPIN, HouseNumber, Street, District, Phone, Expired)
values(31, N'8/30', N'Dương Bá Trạc', N'6', NULL, '2015-05-10')
insert AdultReader (ReaderPIN, HouseNumber, Street, District, Phone, Expired)
values(12, N'20', N'Mai Thị Lựu', N'4', NULL, '2015-07-24')
insert AdultReader (ReaderPIN, HouseNumber, Street, District, Phone, Expired)
values(88, N'21/44', N'An Dương Vương', N'6', NULL, '2015-01-27')
insert AdultReader (ReaderPIN, HouseNumber, Street, District, Phone, Expired)
values(7, N'70', N'Nguyễn Trãi', N'GV', NULL, '2015-07-02')
--Children reader-----
INSERT INTO ChildrenReader([ReaderPIN],[GuaranteeReader])
VALUES('98','65')
INSERT INTO ChildrenReader([ReaderPIN],[GuaranteeReader])
VALUES('96','65')
INSERT INTO ChildrenReader([ReaderPIN],[GuaranteeReader])
VALUES('66','105')


-------------BookTitle ----------
INSERT INTO BookTitle([id],[TitleName],[Author],[Summary])
VALUES (17,'Narcisse va Goldmund','Hermann Hesse', N'Tóm Tắt' )

INSERT INTO BookTitle([id],[TitleName],[Author],[Summary])
VALUES (12,'The Village Watch-Tower','Kate Douglas Wiggin', N'Tóm Tắt' )

INSERT INTO BookTitle([id],[TitleName],[Author],[Summary])
VALUES (19,N'Từ Hy Thái Hậu','Pearl S.Buck', N'Tóm Tắt' )

INSERT INTO BookTitle([id],[TitleName],[Author],[Summary])
VALUES (2,'The Night-Born','Jack London', N'Tóm Tắt' )

INSERT INTO BookTitle([id],[TitleName],[Author],[Summary])
VALUES (14,N'Nữ Kiệt Tổng Khánh Linh',N'Tường Hồng Bang', N'Tóm Tắt' )

INSERT INTO BookTitle([id],[TitleName],[Author],[Summary])
VALUES (15,N'Trang sức người Viết có',N'Trịnh Sinh', N'Tóm Tắt' )

INSERT INTO BookTitle([id],[TitleName],[Author],[Summary])
VALUES (5,N'Án mây dĩ vãng','Chu Lai', N'Tóm Tắt' )

INSERT INTO BookTitle([id],[TitleName],[Author],[Summary])
VALUES (26,'The Call of the Wild','Jack London', N'Tóm Tắt' )

INSERT INTO BookTitle([id],[TitleName],[Author],[Summary])
VALUES (50,'Wayfarers','Knut Hamsun', N'Tóm Tắt' )

INSERT INTO BookTitle([id],[TitleName],[Author],[Summary])
VALUES (16,N'Agélique & vương quốc Ả Rập','Sergeanne Golon', N'Tóm Tắt' )

INSERT INTO BookTitle([id],[TitleName],[Author],[Summary])
VALUES (29,N'Keep calm and meow on','HahaTTpro', N'Tóm Tắt' )

INSERT INTO BookTitle([id],[TitleName],[Author],[Summary])
VALUES (456,N'Con Mèo kêu Meow Meow','Thái Thiện', N'Ngày xủa ngày xưa có một con mèo. Nó mập ú. Hết. Meow' )

---------------BookEdition
INSERT INTO BookEdition([ISBN],[TitleID],[ELanguage],[Cover],[EStatus])
VALUES (969,14,'Vietnamese','Soft Cover','Y')

INSERT INTO BookEdition([ISBN],[TitleID],[ELanguage],[Cover],[EStatus])
VALUES (720,5,'Italian','Soft Cover','Y')
INSERT INTO BookEdition([ISBN],[TitleID],[ELanguage],[Cover],[EStatus])
VALUES (721,5,'Vietnamese','Soft Cover','Y')
INSERT INTO BookEdition([ISBN],[TitleID],[ELanguage],[Cover],[EStatus])
VALUES (722,5,'Chinese','Soft Cover','Y')
INSERT INTO BookEdition([ISBN],[TitleID],[ELanguage],[Cover],[EStatus])
VALUES (723,5,'German','Soft Cover','Y')
INSERT INTO BookEdition([ISBN],[TitleID],[ELanguage],[Cover],[EStatus])
VALUES (724,5,'English','Soft Cover','Y')
INSERT INTO BookEdition([ISBN],[TitleID],[ELanguage],[Cover],[EStatus])
VALUES (725,5,'Japanese','Soft Cover','Y')

INSERT INTO BookEdition([ISBN],[TitleID],[ELanguage],[Cover],[EStatus])
VALUES (625,19,'Chinese','Soft Cover','Y')

INSERT INTO BookEdition([ISBN],[TitleID],[ELanguage],[Cover],[EStatus])
VALUES (69,15,'Vietnamese','Hard Cover','Y')

INSERT INTO BookEdition([ISBN],[TitleID],[ELanguage],[Cover],[EStatus])
VALUES (13,2,'German','Hard Cover','Y')

INSERT INTO BookEdition([ISBN],[TitleID],[ELanguage],[Cover],[EStatus])
VALUES (225,26,'Vietnamese','Hard Cover','Y')

INSERT INTO BookEdition([ISBN],[TitleID],[ELanguage],[Cover],[EStatus])
VALUES (5555,456,'Vietnamese','Soft Cover','N')

insert BookEdition (ISBN, TitleID, ELanguage, Cover, EStatus)
values (26, 2, N'German', N'Hard cover', N'Y')
insert BookEdition (ISBN, TitleID, ELanguage, Cover, EStatus)
values (50, 29, N'English', N'Soft cover', N'Y')
insert BookEdition (ISBN, TitleID, ELanguage, Cover, EStatus)
values (16, 5, N'Japanese', N'Hard cover', N'Y')
---------Book Copy 
INSERT INTO BookCopy([ISBN],[CopyNum],[CStatus])
VALUES (969,3,'N')

INSERT INTO BookCopy([ISBN],[CopyNum],[CStatus])
VALUES (720,2,'N')

INSERT INTO BookCopy([ISBN],[CopyNum],[CStatus])
VALUES (625,3,'Y')

INSERT INTO BookCopy([ISBN],[CopyNum],[CStatus])
VALUES (625,2,'N')

INSERT INTO BookCopy([ISBN],[CopyNum],[CStatus])
VALUES (625,1,'Y')

INSERT INTO BookCopy([ISBN],[CopyNum],[CStatus])
VALUES (969,4,'N')

INSERT INTO BookCopy([ISBN],[CopyNum],[CStatus])
VALUES (69,3,'N')

INSERT INTO BookCopy([ISBN],[CopyNum],[CStatus])
VALUES (13,3,'N')

INSERT INTO BookCopy([ISBN],[CopyNum],[CStatus])
VALUES (225,3,'N')
INSERT INTO BookCopy([ISBN],[CopyNum],[CStatus])
VALUES (225,4,'Y')
INSERT INTO BookCopy([ISBN],[CopyNum],[CStatus])
VALUES (225,5,'Y')

INSERT INTO BookCopy([ISBN],[CopyNum],[CStatus])
VALUES (5555,4,'Y')

insert BookCopy (ISBN, CopyNum, CStatus)
values (969, 9, N'N')
insert BookCopy (ISBN, CopyNum, CStatus)
values (720, 10, N'N')
insert BookCopy (ISBN, CopyNum, CStatus)
values (625, 7, N'Y')
insert BookCopy (ISBN, CopyNum, CStatus)
values (69, 7, N'N')
insert BookCopy (ISBN, CopyNum, CStatus)
values (69, 8, N'N')
insert BookCopy (ISBN, CopyNum, CStatus)
values (69, 9, N'N')
insert BookCopy (ISBN, CopyNum, CStatus)
values (13, 8, N'N')
insert BookCopy (ISBN, CopyNum, CStatus)
values (225, 11, N'N')
insert BookCopy (ISBN, CopyNum, CStatus)
values (225, 12, N'N')
insert BookCopy (ISBN, CopyNum, CStatus)
values (26, 7, N'N')
insert BookCopy (ISBN, CopyNum, CStatus)
values (50, 8, N'N')
insert BookCopy (ISBN, CopyNum, CStatus)
values (16, 9, N'N')
insert BookCopy (ISBN, CopyNum, CStatus)
values (16, 8, N'N')

insert BookCopy (ISBN, CopyNum, CStatus)
values (13, 13, N'Y')
insert BookCopy (ISBN, CopyNum, CStatus)
values (16, 13, N'Y')
insert BookCopy (ISBN, CopyNum, CStatus)
values (26, 13, N'Y')
insert BookCopy (ISBN, CopyNum, CStatus)
values (50, 13, N'Y')
insert BookCopy (ISBN, CopyNum, CStatus)
values (69, 13, N'Y')
insert BookCopy (ISBN, CopyNum, CStatus)
values (225, 13, N'Y')
insert BookCopy (ISBN, CopyNum, CStatus)
values (625, 13, N'Y')
insert BookCopy (ISBN, CopyNum, CStatus)
values (720, 13, N'Y')
insert BookCopy (ISBN, CopyNum, CStatus)
values (969, 13, N'Y')
insert BookCopy (ISBN, CopyNum, CStatus)
values (5555, 13, N'Y')
insert BookCopy (ISBN, CopyNum, CStatus)
values (5555, 5, N'Y')
insert BookCopy (ISBN, CopyNum, CStatus)
values (5555, 6, N'Y')
-----------Booking
INSERT INTO Booking([ISBN],[ReaderPIN],[BookingDate],[Note])
VALUES (969,65,'2012-03-03',NULL)
INSERT INTO Booking([ISBN],[ReaderPIN],[BookingDate],[Note])
VALUES (720,105,'2012-03-03',NULL)
INSERT INTO Booking([ISBN],[ReaderPIN],[BookingDate],[Note])
VALUES (5555,66,'2012-03-03','meow meow meow meow')
INSERT INTO Booking([ISBN],[ReaderPIN],[BookingDate],[Note])
VALUES (969,105,'2012-03-03',NULL)
INSERT INTO Booking([ISBN],[ReaderPIN],[BookingDate],[Note])
VALUES (5555,65,'2012-03-03',N'Mèo mập dễ thương quá, meow')

-----------borrowing
INSERT INTO Borrowing([ISBN],[BCopyNum],[ReaderPIN],[BorrowDate],[MustReturnDate])
VALUES(969,3,65,'2014-07-05 00:00:00','2015-07-19 00:00:00')
INSERT INTO Borrowing([ISBN],[BCopyNum],[ReaderPIN],[BorrowDate],[MustReturnDate])
VALUES(5555,4,66,'2014-05-16 00:00:00','2015-03-05 00:00:00')
INSERT INTO Borrowing([ISBN],[BCopyNum],[ReaderPIN],[BorrowDate],[MustReturnDate])
VALUES(225,3,66,'2014-05-14 00:00:00','2015-03-05 00:00:00')
INSERT INTO Borrowing([ISBN],[BCopyNum],[ReaderPIN],[BorrowDate],[MustReturnDate])
VALUES(225,5,98,'2014-05-14 00:00:00','2015-03-05 00:00:00')
INSERT INTO Borrowing([ISBN],[BCopyNum],[ReaderPIN],[BorrowDate],[MustReturnDate])
VALUES(5555,6,99,'2014-05-14 00:00:00','2015-03-05 00:00:00')

-- one man borrow them all
INSERT INTO Borrowing([ISBN],[BCopyNum],[ReaderPIN],[BorrowDate],[MustReturnDate])
VALUES(13,13,113,'2014-05-14 00:00:00','2015-03-05 00:00:00')
INSERT INTO Borrowing([ISBN],[BCopyNum],[ReaderPIN],[BorrowDate],[MustReturnDate])
VALUES(16,13,113,'2014-05-14 00:00:00','2015-03-05 00:00:00')
INSERT INTO Borrowing([ISBN],[BCopyNum],[ReaderPIN],[BorrowDate],[MustReturnDate])
VALUES(26,13,113,'2014-05-14 00:00:00','2015-03-05 00:00:00')
INSERT INTO Borrowing([ISBN],[BCopyNum],[ReaderPIN],[BorrowDate],[MustReturnDate])
VALUES(50,13,113,'2014-05-14 00:00:00','2015-03-05 00:00:00')
INSERT INTO Borrowing([ISBN],[BCopyNum],[ReaderPIN],[BorrowDate],[MustReturnDate])
VALUES(69,13,113,'2014-05-14 00:00:00','2015-03-05 00:00:00')
INSERT INTO Borrowing([ISBN],[BCopyNum],[ReaderPIN],[BorrowDate],[MustReturnDate])
VALUES(225,13,113,'2014-05-14 00:00:00','2015-03-05 00:00:00')

-- cuốn sách duy nhất nó mà thằng mượn hết sách nó éo mượn được tại vì thằng cuồng Từ hy thái hậu đã ôm hết tất cả các BookCopy
--INSERT INTO Borrowing([ISBN],[BCopyNum],[ReaderPIN],[BorrowDate],[MustReturnDate])
--VALUES(625,13,113,'2014-05-14 00:00:00','2015-03-05 00:00:00')

INSERT INTO Borrowing([ISBN],[BCopyNum],[ReaderPIN],[BorrowDate],[MustReturnDate])
VALUES(720,13,113,'2014-05-14 00:00:00','2015-03-05 00:00:00')
INSERT INTO Borrowing([ISBN],[BCopyNum],[ReaderPIN],[BorrowDate],[MustReturnDate])
VALUES(969,13,113,'2014-05-14 00:00:00','2015-03-05 00:00:00')
INSERT INTO Borrowing([ISBN],[BCopyNum],[ReaderPIN],[BorrowDate],[MustReturnDate])
VALUES(5555,13,113,'2014-05-14 00:00:00','2015-03-05 00:00:00')
INSERT INTO Borrowing([ISBN],[BCopyNum],[ReaderPIN],[BorrowDate],[MustReturnDate])
VALUES(5555,5,65,'2014-05-14 00:00:00','2015-03-05 00:00:00')

-- borrow all tu hy thai hau
INSERT INTO Borrowing([ISBN],[BCopyNum],[ReaderPIN],[BorrowDate],[MustReturnDate])
VALUES(625,1,114,'2014-05-14 00:00:00','2015-03-05 00:00:00')
INSERT INTO Borrowing([ISBN],[BCopyNum],[ReaderPIN],[BorrowDate],[MustReturnDate])
VALUES(625,2,114,'2014-05-14 00:00:00','2015-03-05 00:00:00')
INSERT INTO Borrowing([ISBN],[BCopyNum],[ReaderPIN],[BorrowDate],[MustReturnDate])
VALUES(625,3,114,'2014-05-14 00:00:00','2015-03-05 00:00:00')
INSERT INTO Borrowing([ISBN],[BCopyNum],[ReaderPIN],[BorrowDate],[MustReturnDate])
VALUES(625,7,114,'2014-05-14 00:00:00','2015-03-05 00:00:00')

-- thằng mượn hết sách sẽ mượn luôn cuốn này, đổi 114 lại thành 113 để thằng mượn hết sách nó mượn được hết sách
INSERT INTO Borrowing([ISBN],[BCopyNum],[ReaderPIN],[BorrowDate],[MustReturnDate])
VALUES(625,13,114,'2014-05-14 00:00:00','2015-03-05 00:00:00')


---------BorrowedHistory
INSERT INTO BorrowedHistory([ISBN],[BookCopyNum],[BorrowDate],[ReaderPin],[MustReturnDate],[ReturnDate],[LateFine],[Note])
VALUES (5555,4,'2013-08-30 00:00:00',66,'2013-09-13 00:00:00','2013-09-02 00:00:00',NULL,NULL)
INSERT INTO BorrowedHistory([ISBN],[BookCopyNum],[BorrowDate],[ReaderPin],[MustReturnDate],[ReturnDate],[LateFine],[Note])
VALUES (5555,4,'2013-08-28 00:00:00',65,'2014-09-13 00:00:00','2014-09-15 00:00:00',15000,'keep meow and pay late fine')
INSERT INTO BorrowedHistory([ISBN],[BookCopyNum],[BorrowDate],[ReaderPin],[MustReturnDate],[ReturnDate],[LateFine],[Note])
VALUES (625,1,'2015-01-02 00:00:00',66,'2015-01-17 00:00:00','2015-01-17 00:00:00',NULL,NULL)

insert BorrowedHistory (ISBN, BookCopyNum, BorrowDate, ReaderPin, MustReturnDate, ReturnDate, LateFine, Note)
values (16, 9, '2013-08-03 00:00:00'  , 65, '2013-09-13 00:00:00', '2013-09-02 00:00:00', NULL, NULL)
insert BorrowedHistory (ISBN, BookCopyNum, BorrowDate, ReaderPin, MustReturnDate, ReturnDate, LateFine, Note)
values (50, 8, '2014-02-27 00:00:00' , 105, '2014-03-13 00:00:00', '2014-03-01 00:00:00', NULL, NULL)
insert BorrowedHistory (ISBN, BookCopyNum, BorrowDate, ReaderPin, MustReturnDate, ReturnDate, LateFine, Note)
values (26, 7, '2015-01-02 00:00:00' , 98, '2015-01-17 00:00:00', '2015-01-17 00:00:00', NULL, NULL)
insert BorrowedHistory (ISBN, BookCopyNum, BorrowDate, ReaderPin, MustReturnDate, ReturnDate, LateFine, Note)
values (225, 12, '2013-06-06 00:00:00' , 96, '2015-06-30 00:00:00', '2015-06-30 00:00:00', NULL, NULL)
insert BorrowedHistory (ISBN, BookCopyNum, BorrowDate, ReaderPin, MustReturnDate, ReturnDate, LateFine, Note)
values (225, 11, '2013-06-07 00:00:00' ,  23, '2013-06-21 00:00:00', '2013-06-10 00:00:00', NULL, NULL)
insert BorrowedHistory (ISBN, BookCopyNum, BorrowDate, ReaderPin, MustReturnDate, ReturnDate, LateFine, Note)
values (16, 8, '2014-12-01 00:00:00' , 31, '2014-12-25 00:00:00', '2014-12-25 00:00:00', NULL, NULL)
insert BorrowedHistory (ISBN, BookCopyNum, BorrowDate, ReaderPin, MustReturnDate, ReturnDate, LateFine, Note)
values (13, 8, '2014-06-06 00:00:00' , 66, '2014-06-30 00:00:00', '2014-05-30 00:00:00', NULL, NULL)
insert BorrowedHistory (ISBN, BookCopyNum, BorrowDate, ReaderPin, MustReturnDate, ReturnDate, LateFine, Note)
values (69, 9, '2013-09-26 00:00:00' , 12, '2013-10-10 00:00:00', '2013-09-28 00:00:00', NULL, NULL)
insert BorrowedHistory (ISBN, BookCopyNum, BorrowDate, ReaderPin, MustReturnDate, ReturnDate, LateFine, Note)
values (69, 8, '2013-11-07 00:00:00' , 88, '2013-11-21 00:00:00', '2013-11-09 00:00:00', NULL, NULL)
insert BorrowedHistory (ISBN, BookCopyNum, BorrowDate, ReaderPin, MustReturnDate, ReturnDate, LateFine, Note)
values (69, 7, '2014-05-05 00:00:00' , 7, '2014-05-25 00:00:00', '2014-05-25 00:00:00', NULL, NULL)

insert BorrowedHistory (ISBN, BookCopyNum, BorrowDate, ReaderPin, MustReturnDate, ReturnDate, LateFine, Note)
values (5555, 6, '2014-05-05 00:00:00' , 99, '2014-05-25 00:00:00', '2014-05-25 00:00:00', NULL, NULL)

--retrieve the number of available books.
SELECT count(*)
FROM BookCopy as bc
WHERE bc.CStatus='Y'


--retreive information of children reader and guarantee reader
SELECT r1.*,r2.*
FROM Reader as r1, Reader r2, AdultReader as ar, ChildrenReader as cr
WHERE r1.PIN = ar.ReaderPIN
AND	r2.PIN=cr.ReaderPIN
AND cr.GuaranteeReader = ar.ReaderPIN

--find the number of books which was borrowed by children
SELECT count(*)
FROM ChildrenReader as cr, BorrowedHistory as bh
WHERE cr.ReaderPIN = bh.ReaderPin

----find the number of books which was borrowed by adult
SELECT count(*)
FROM AdultReader as ar, BorrowedHistory as bh
WHERE ar.ReaderPIN = bh.ReaderPin

--retrieve all books which are being borrowed by adult
SELECT b.ISBN, bt.TitleName,bt.Author
FROM AdultReader as ar, Borrowing as b, BookEdition as be,BookTitle as bt
WHERE ar.ReaderPIN = b.ReaderPIN
and   b.ISBN = be.ISBN
and be.TitleID = bt.id


--retrieve books which were borrowed by adult
SELECT distinct b.ISBN, bt.TitleName, bt.Author 
FROM AdultReader as ar, BorrowedHistory as b, BookEdition as be, BookTitle as bt 
WHERE ar.ReaderPIN = b.ReaderPIN
and b.ISBN = be.ISBN
and be.TitleID = bt.id


--find the number of kinds of books 


-- list all kind of books
SELECT be.ISBN,bt.TitleName,bt.Author
FROM BookTitle as bt, BookEdition as be
WHERE be.TitleID = bt.id


--retrieve the information of readers who are borrowing all kinds of book
--> retrieve the information of readers dont have any book that he is not borrowing
SELECT r.*
FROM Reader as r
WHERE
-- dont have any book
 not exists( 
SELECT be.ISBN
FROM BookEdition as be
WHERE --he is not borrow 
not exists (		SELECT be.ISBN
					FROM Borrowing as b
					WHERE b.ReaderPIN = r.PIN
					and b.ISBN=be.ISBN
					)
)


--find the number of books which are being borrowed by 'Hà'
SELECT count(*)
FROM BookEdition as be, Reader as r, Borrowing as b
WHERE b.ReaderPIN = r.PIN
and concat(r.FName,' ',r.MName,' ',r.LName) like N'%Hà%'
and b.ISBN = be.ISBN

--find the number of copies of 'Từ hy thái hậu' book
SELECT bc.CopyNum
FROM BookCopy as bc, BookTitle as bt, BookEdition as be
WHERE bc.ISBN = be.ISBN
and be.TitleID = bt.id
and bt.TitleName = N'Từ hy thái hậu'


--retrieve the information of reader who is borrowing all copy of 'Từ hy thái hậu'
--retrieve the information of reader who dont have any copy of 'Từ hy thái hậu'  that he is not borrowing
Select r.*
From Reader as r 
Where not exists (SELECT bc.CopyNum
					FROM BookCopy as bc, BookTitle as bt, BookEdition as be 
					WHERE bt.TitleName = N'Từ hy thái hậu'
					and bt.id = be.TitleID 
					and be.ISBN = bc.ISBN
					and not exists(	SELECT bc.CopyNum
									FROM Borrowing as b
									WHERE bt.TitleName = N'Từ hy thái hậu'
									and bt.id = be.TitleID 
									and be.ISBN = bc.ISBN
									and b.BCopyNum = bc.CopyNum
									and b.ISBN = bc.ISBN
									and b.ReaderPIN = r.PIN
									
									)
)


------------
--List all children reader's information (Reader PIN, Lastname, Firstname, age). The result is sorted by reader's age in descending. 
--The age is calculated by this formule (Current year - Year of Reader's DoB)

SELECT r.PIN, r.LName, r.FName, ( YEAR(GETDATE())-YEAR(r.DoB))  AS age
FROM Reader as r, ChildrenReader as cr
WHERE r.PIN = cr.ReaderPIN 
ORDER BY age DESC

--For each adult reader, list the reader information (Reader PIN, lastname, firstname,
-- and the number of reader children whom that adult reader guarantees).
-- The result list is sorted by the number of children in ascending.
SELECT r.PIN,r.LName,r.FName, count(*) as NumOfChildren
FROM Reader as r, AdultReader as ad, ChildrenReader as cr 
WHERE r.PIN = ad.ReaderPIN
and cr.GuaranteeReader = r.PIN
GROUP BY r.PIN,r.LName,r.FName
ORDER BY NumOfChildren asc

--List all adult readers (Lastname, Firstname, Number of guaranteed children reader) who have the least guaranteed children.
-- The result list will be sorted by lastname in ascending
SELECT r.PIN,r.LName,r.FName, count(*) as NumOfChildren
FROM Reader as r, AdultReader as ad, ChildrenReader as cr 
WHERE r.PIN = ad.ReaderPIN
and cr.GuaranteeReader = r.PIN
GROUP BY r.PIN,r.LName,r.FName
Having count(*) =1
ORDER BY r.LName asc

--Retrieve the number of adult readers who have age nearby the average age of the adult reader in library. 
--(Nearby age: more or less than 2. Age is calculated by (current year - year of DoB). Use abs(N) function to get the absolute of N)
SELECT count(distinct r.PIN)
FROM Reader as r, AdultReader as ad
WHERE r.PIN = ad.ReaderPIN
and ABS(( YEAR(GETDATE())-YEAR(r.DoB)) - (SELECT AVG ( YEAR(GETDATE())-YEAR(r.DoB))
											FROM Reader as r, AdultReader as ad
											WHERE r.PIN = ad.ReaderPIN
											))< 3

-- List all book title information (Title Id, title name, author name) which has all languages in system for its book editions.
-- Sort the result by name of the book title in ascending.
-- list all title which don't have any language in system it dont have in it book edition
SELECT bt.id, bt.TitleName, bt.Author
FROM BookTitle as bt
WHERE not exists(
-- which dont have any languae in system
SELECT distinct be.ELanguage
FROM BookEdition as be
WHERE not exists (	SELECT be.ELanguage
					WHERE be.TitleID = bt.id
					)

)

SELECT bt.id, bt.TitleName, bt.Author,count(distinct ed.ELanguage)
FROM BookTitle as bt, BookEdition as ed
WHERE bt.id = ed.TitleID
GROUP BY  bt.id, bt.TitleName, bt.Author, ed.ELanguage
HAVING count(distinct ed.ELanguage)


--List the adult reader's information (Reader PIN, Lastname, Firstname, expired date) whose PIN has at least 1 day or maximum of 15 days before his expired date.
-- The result list is sorted by the number of days before expired date in descending.

SELECT r.PIN,r.LName,r.FName,ad.Expired
FROM Reader as r,AdultReader as ad
WHERE r.PIN = ad.ReaderPIN
and  (DAY(GETDATE()) - DAY(ad.Expired)) <16
and  (DAY(GETDATE()) - DAY(ad.Expired)) >0
ORDER BY (DAY(GETDATE()) - DAY(ad.Expired)) DESC


--------
--List all book title information (Title Id, title name, author name) which has all languages in system for its book editions. 
--Sort the result by name of the book title in ascending.
SELECT bt.id,bt.TitleName, bt.Author,ed.ELanguage
FROM BookTitle as bt, BookEdition as ed
WHERE bt.id = ed.TitleID

SELECT distinct ed.ELanguage
FROM BookEdition as ed


SELECT bt.id,bt.TitleName, bt.Author
FROM BookTitle as bt, BookEdition as ed
WHERE bt.id = ed.TitleID
GROUP BY bt.id,bt.TitleName, bt.Author
HAVING count(distinct ed.ELanguage)= (SELECT count( distinct ed.ELanguage)
										FROM BookEdition as ed)
ORDER BY bt.TitleName asc

/*List the reader information (Reader PIN, lastname, firstname) who is borrowing all book titles which reader 'Lê V Sang' borrowed. Sort the result by lastname in ascending.
[2:32:20 PM] Mai Ho: m lam dum t cau nay voi*/


SELECT bt.TitleName
FROM Reader as r, BorrowedHistory as bh, BookTitle as bt, BookEdition as be 
where 
r.PIN = bh.ReaderPin
and bh.ISBN = be.ISBN
and be.TitleID = bt.id
and concat (r.LName,' ',r.MName,' ',r.FName) = N'Lê V Sang' 

---


SELECT r.PIN,r.LName,r.FName
FROM Reader as r, Borrowing as b, BookTitle as bt, BookEdition as be, BorrowedHistory as bh, Reader r2
WHERE 
r.PIN = b.ReaderPIN
and bt.id = be.TitleID
and be.ISBN = b.ISBN
and concat (r2.LName,' ',r2.MName,' ',r2.FName) = N'Lê V Sang' 
and r2.PIN = bh.ReaderPin
and bh.ISBN = be.ISBN
and be.TitleID = bt.id
GROUP BY 

--Retrieve the number of adult readers who have age nearby the average age of the adult reader in library. (Nearby age: more or less than 2. Age is calculated by (current year - year of DoB). Use abs(N) function to get the absolute of N)