create database MidtermExam
go

Use  MidtermExam
go

create table Reader(
	PIN smallint primary key,
	LName nvarchar(15) NOT NULL,
	MName nvarchar(1),
	FName nvarchar(15) NOT NULL,
	DoB date,
)



create table AdultReader(
	ReaderPIN smallint primary key,
	HouseNumber nvarchar(15) NOT NULL,
	Street nvarchar(63) NOT NULL,
	District nvarchar(2) NOT NULL,
	Phone nvarchar(13) ,
	Expired date NOT NULL,
)



create table ChildrenReader(
	ReaderPIN smallint primary key,
	GuaranteeReader smallint NOT NULL,
)



create table BookCopy(
	ISBN int,
	CopyNum smallint,
	CStatus nvarchar(1),
	primary key (ISBN, CopyNum)
)

create table BorrowedHistory(
	ISBN int,
	BookCopyNum smallint,
	BorrowDate smalldatetime,
	ReaderPin smallint NOT NULL,
	MustReturnDate smalldatetime,
	ReturnDate smalldatetime,
	LateFine money,
	Note nvarchar(255),
	primary key(ISBN,BookCopyNum,BorrowDate)
)

create table Borrowing(
	ISBN int,
	BCopyNum smallint,
	ReaderPIN smallint,
	BorrowDate smalldatetime,
	MustReturnDate smalldatetime,
	primary key (ISBN, BCopyNum)
)



create table Booking(
	ISBN int,
	ReaderPIN smallint,
	BookingDate smalldatetime,
	Note nvarchar(255),
	primary key (ISBN, ReaderPIN),
)



create table BookEdition(
	ISBN int primary key,
	TitleID int,
	ELanguage nvarchar(15),
	Cover nvarchar(15),
	EStatus nvarchar(1),

)

create table BookTitle(
	id int primary key,
	TitleName nvarchar(63),
	Author nvarchar(31),
	Summary nvarchar(222)
)




---------------
insert Reader (PIN, LName, MName, FName, DoB)
values (65, N'Dương', N'L', N'Hà', '1981-08-30')
insert Reader (PIN, LName, MName, FName, DoB)
values (105, N'Mạc', N'L' , N'Lan', '1981-09-01')
insert Reader (PIN, LName, MName, FName, DoB)
values (98, N'Bùi', N'H', N'Sang', '2004-02-29')
insert Reader (PIN, LName, MName, FName, DoB)
values (96, N'Vũ', N'T', N'Anh', '2005-06-29')
insert Reader (PIN, LName, MName, FName, DoB)
values (23, N'Đặng', N'P', N'Duyên', '1990-06-09')
insert Reader (PIN, LName, MName, FName, DoB)
values (31, N'Phạm', N'L', N'Nam', '1985-06-09')
insert Reader (PIN, LName, MName, FName, DoB)
values (66, N'Lê', N'T', N'Khang', '2004-09-09')
insert Reader (PIN, LName, MName, FName, DoB)
values (12, N'Đỗ', N'B', N'Phương', '2006-07-15')
insert Reader (PIN, LName, MName, FName, DoB)
values (88, N'Lê', N'H', N'Linh', '2003-12-30')
insert Reader (PIN, LName, MName, FName, DoB)
values (7, N'Lê', N'B', N'Mộng', '1990-03-06')


--------------------
insert AdultReader (ReaderPIN, HouseNumber, Street, District, Phone, Expired)
values(93, N'70', N'Bùi Thị Xuân', N'GV', NULL, '2015-09-20')
insert AdultReader (ReaderPIN, HouseNumber, Street, District, Phone, Expired)
values(9, N'50bit', N'Lê Lợi', N'10', NULL, '2015-06-28')
insert AdultReader (ReaderPIN, HouseNumber, Street, District, Phone, Expired)
values(37, N'3/', N'Nguyễn Văn Cừ', N'10', NULL, '2015-05-20')
insert AdultReader (ReaderPIN, HouseNumber, Street, District, Phone, Expired)
values(65, N'10', N'Trần Hưng Đạo', N'2', NULL, '2015-08-23')
insert AdultReader (ReaderPIN, HouseNumber, Street, District, Phone, Expired)
values(67, N'20', N'Trần Phú', N'4', NULL, '2015-08-25')
insert AdultReader (ReaderPIN, HouseNumber, Street, District, Phone, Expired)
values(59, N'60', N'Nguyễn Trãi', N'BT', NULL, '2015-08-17')
insert AdultReader (ReaderPIN, HouseNumber, Street, District, Phone, Expired)
values(5, N'8/30', N'Dương Bá Trạc', N'6', NULL, '2015-05-10')
insert AdultReader (ReaderPIN, HouseNumber, Street, District, Phone, Expired)
values(35, N'20', N'Mai Thị Lựu', N'4', NULL, '2015-07-24')
insert AdultReader (ReaderPIN, HouseNumber, Street, District, Phone, Expired)
values(69, N'21/44', N'An Dương Vương', N'6', NULL, '2015-01-27')
insert AdultReader (ReaderPIN, HouseNumber, Street, District, Phone, Expired)
values(13, N'70', N'Nguyễn Trãi', N'GV', NULL, '2015-07-02')

-----------------------------------
insert ChildrenReader (ReaderPIN, GuaranteeReader)
values (20, 19)
insert ChildrenReader (ReaderPIN, GuaranteeReader)
values (64, 63)
insert ChildrenReader (ReaderPIN, GuaranteeReader)
values (78, 77)
insert ChildrenReader (ReaderPIN, GuaranteeReader)
values (70, 69)
insert ChildrenReader (ReaderPIN, GuaranteeReader)
values (18, 17)
insert ChildrenReader (ReaderPIN, GuaranteeReader)
values (102, 1)
insert ChildrenReader (ReaderPIN, GuaranteeReader)
values (54, 53)
insert ChildrenReader (ReaderPIN, GuaranteeReader)
values (86, 85)
insert ChildrenReader (ReaderPIN, GuaranteeReader)
values (58, 57)
insert ChildrenReader (ReaderPIN, GuaranteeReader)
values (16, 15)

insert BookTitle (id, TitleName, Author, Summary)
values (17, N'Narcisse va Goldmund', N'Hemann Hesse', N'Tóm tắt...')
insert BookTitle (id, TitleName, Author, Summary)
values (12, N'The Village Watch-Tower', N'Kate Douglas Wiggin', N'Tóm tắt...')
insert BookTitle (id, TitleName, Author, Summary)
values (19, N'Từ hy thái hậu', N'Pearl S.Buck', N'Tóm tắt...')
insert BookTitle (id, TitleName, Author, Summary)
values (2, N'The Night-Born', N'Jack London', N'Tóm tắt...')
insert BookTitle (id, TitleName, Author, Summary)
values (14, N'Nữ kiệt Tống Khánh Linh', N'Tương Hồng Bang', N'Tóm tắt...')
insert BookTitle (id, TitleName, Author, Summary)
values (15, N'Trang sức người Việt cổ', N'Trịnh Sinh', N'Tóm tắt...')
insert BookTitle (id, TitleName, Author, Summary)
values (5, N'Án mây dĩ vãng', N'Chu Lai', N'Tóm tắt...')
insert BookTitle (id, TitleName, Author, Summary)
values (26, N'The Call of the Wild', N'Jack London', N'Tóm tắt...')
insert BookTitle (id, TitleName, Author, Summary)
values (50, N'Wayfares', N'Knut Hamsun', N'Tóm tắt...')
insert BookTitle (id, TitleName, Author, Summary)
values (16, N'Age1lique & vương quốc Ả Rập', N'Sergeanne Golon', N'Tóm tắt...')



insert BookEdition (ISBN, TitleID, ELanguage, Cover, ELanguage)
values (969, 47, N'Vietnamese', N'Soft cover', N'Y')
insert BookEdition (ISBN, TitleID, ELanguage, Cover, ELanguage)
values (720, 22, N'Italian', N'Soft cover', N'Y')
insert BookEdition (ISBN, TitleID, ELanguage, Cover, ELanguage)
values (625, 13, N'Chinese', N'Soft cover', N'Y')
insert BookEdition (ISBN, TitleID, ELanguage, Cover, ELanguage)
values (825, 66, N'Chinese', N'Soft cover', N'N')
insert BookEdition (ISBN, TitleID, ELanguage, Cover, ELanguage)
values (69, 7, N'Vietnamese', N'Hard cover', N'Y')
insert BookEdition (ISBN, TitleID, ELanguage, Cover, ELanguage)
values (810, 31, N'Italian', N'Soft cover', N'Y')
insert BookEdition (ISBN, TitleID, ELanguage, Cover, ELanguage)
values (643, 15, N'German', N'Soft cover', N'Y')
insert BookEdition (ISBN, TitleID, ELanguage, Cover, ELanguage)
values (13, 2, N'German', N'Hard cover', N'Y')
insert BookEdition (ISBN, TitleID, ELanguage, Cover, ELanguage)
values (781, 29, N'English', N'Soft cover', N'Y')
insert BookEdition (ISBN, TitleID, ELanguage, Cover, ELanguage)
values (46, 5, N'Japanese', N'Hard cover', N'Y')

insert BookCopy (ISBN, CopyNum, CStatus)
values (977, 3, N'N')
insert BookCopy (ISBN, CopyNum, CStatus)
values (411, 2, N'N')
insert BookCopy (ISBN, CopyNum, CStatus)
values (391, 3, N'Y')
insert BookCopy (ISBN, CopyNum, CStatus)
values (876, 5, N'N')
insert BookCopy (ISBN, CopyNum, CStatus)
values (239, 4, N'N')
insert BookCopy (ISBN, CopyNum, CStatus)
values (636, 5, N'N')
insert BookCopy (ISBN, CopyNum, CStatus)
values (367, 3, N'N')
insert BookCopy (ISBN, CopyNum, CStatus)
values (379, 4, N'N')
insert BookCopy (ISBN, CopyNum, CStatus)
values (905, 5, N'N')
insert BookCopy (ISBN, CopyNum, CStatus)
values (554, 2, N'N')

insert Booking (ISBN, ReaderPIN,BookingDate, Note)
values (617, 86, '2012-03-03 00:00:00', NULL)
insert Booking (ISBN, ReaderPIN,BookingDate, Note)
values (246, 18, '2012-03-03 00:00:00', NULL)
insert Booking (ISBN, ReaderPIN,BookingDate, Note)
values (1, 52, '2012-03-03 00:00:00', NULL)
insert Booking (ISBN, ReaderPIN,BookingDate, Note)
values (330, 35, '2012-03-03 00:00:00', NULL)
insert Booking (ISBN, ReaderPIN,BookingDate, Note)
values (1, 1, '2012-03-03 00:00:00', NULL)
insert Booking (ISBN, ReaderPIN,BookingDate, Note)
values (533, 18, '2012-03-03 00:00:00', NULL)
insert Booking (ISBN, ReaderPIN,BookingDate, Note)
values (533, 69, '2012-03-03 00:00:00', NULL)
insert Booking (ISBN, ReaderPIN,BookingDate, Note)
values (330, 86, '2012-03-03 00:00:00', NULL)
insert Booking (ISBN, ReaderPIN,BookingDate, Note)
values (43, 86, '2012-03-03 00:00:00', NULL)
insert Booking (ISBN, ReaderPIN,BookingDate, Note)
values (820, 69, '2012-03-03 00:00:00', NULL)


insert Borrowing (ISBN, BCopyNum, ReaderPIN, BorrowDate, MustReturnDate)
values (737, 2, 68, '2014-07-05 00:00:00', '2014-07-19 00:00:00')
insert Borrowing (ISBN, BCopyNum, ReaderPIN, BorrowDate, MustReturnDate)
values (89, 5, 9, '2014-05-16 00:00:00', '2014-03-05 00:00:00')
insert Borrowing (ISBN, BCopyNum, ReaderPIN, BorrowDate, MustReturnDate)
values (630, 4, 58, '2014-07-11 00:00:00', '2014-07-25 00:00:00')
insert Borrowing (ISBN, BCopyNum, ReaderPIN, BorrowDate, MustReturnDate)
values (643, 1, 59,'2014-07-06 00:00:00', '2014-07-20 00:00:00')
insert Borrowing (ISBN, BCopyNum, ReaderPIN, BorrowDate, MustReturnDate)
values (281, 3, 26, '2014-06-14 00:00:00', '2014-06-28 00:00:00')
insert Borrowing (ISBN, BCopyNum, ReaderPIN, BorrowDate, MustReturnDate)
values (487, 2, 45, '2014-07-17 00:00:00', '2014-07-01 00:00:00')
insert Borrowing (ISBN, BCopyNum, ReaderPIN, BorrowDate, MustReturnDate)
values (450, 4, 42, '2014-07-11 00:00:00', '2014-07-25 00:00:00')
insert Borrowing (ISBN, BCopyNum, ReaderPIN, BorrowDate, MustReturnDate)
values (626, 3, 58, '2014-07-09 00:00:00', '2014-07-28 00:00:00')
insert Borrowing (ISBN, BCopyNum, ReaderPIN, BorrowDate, MustReturnDate)
values (340 ,4, 32, '2014-07-08 00:00:00', '2014-07-17 00:00:00')
insert Borrowing (ISBN, BCopyNum, ReaderPIN, BorrowDate, MustReturnDate)
values (176, 3, 17, '2014-06-03 00:00:00', '2014-06-17 00:00:00')


insert BorrowedHistory (ISBN, BookCopyNum, BorrowDate, ReaderPin, MustReturnDate, ReaderPin, ReturnDate, LateFine, Note)
values (1, 3, '2013-08-03 00:00:00'  , 23, '2013-09-13 00:00:00', '2013-09-02 00:00:00', NULL, NULL)
insert BorrowedHistory (ISBN, BookCopyNum, BorrowDate, ReaderPin, MustReturnDate, ReaderPin, ReturnDate, LateFine, Note)
values (17, 2, '2014-02-27 00:00:00' , 56, '2014-03-13 00:00:00', '2014-03-01 00:00:00', NULL, NULL)
insert BorrowedHistory (ISBN, BookCopyNum, BorrowDate, ReaderPin, MustReturnDate, ReaderPin, ReturnDate, LateFine, Note)
values (824, 1, '2015-01-02 00:00:00' , 11, '2015-01-17 00:00:00', '2015-01-17 00:00:00', NULL, NULL)
insert BorrowedHistory (ISBN, BookCopyNum, BorrowDate, ReaderPin, MustReturnDate, ReaderPin, ReturnDate, LateFine, Note)
values (313, 4, '2013-06-06 00:00:00' , 11, '2015-06-30 00:00:00', '2015-06-30 00:00:00', NULL, NULL)
insert BorrowedHistory (ISBN, BookCopyNum, BorrowDate, ReaderPin, MustReturnDate, ReaderPin, ReturnDate, LateFine, Note)
values (1, 3, '2013-06-07 00:00:00' ,  23, '2013-06-21 00:00:00', '2013-06-10 00:00:00', NULL, NULL)
insert BorrowedHistory (ISBN, BookCopyNum, BorrowDate, ReaderPin, MustReturnDate, ReaderPin, ReturnDate, LateFine, Note)
values (115, 1, '2014-12-01 00:00:00' , 11, '2014-12-25 00:00:00', '2014-12-25 00:00:00', NULL, NULL)
insert BorrowedHistory (ISBN, BookCopyNum, BorrowDate, ReaderPin, MustReturnDate, ReaderPin, ReturnDate, LateFine, Note)
values (4, 1, '2014-06-06 00:00:00' , 11, '2014-06-30 00:00:00', '2014-05-30 00:00:00', NULL, NULL)
insert BorrowedHistory (ISBN, BookCopyNum, BorrowDate, ReaderPin, MustReturnDate, ReaderPin, ReturnDate, LateFine, Note)
values (17, 2, '2013-09-26 00:00:00' , 44, '2013-10-10 00:00:00', '2013-09-28 00:00:00', NULL, NULL)
insert BorrowedHistory (ISBN, BookCopyNum, BorrowDate, ReaderPin, MustReturnDate, ReaderPin, ReturnDate, LateFine, Note)
values (17, 2, '2013-11-07 00:00:00' , 5, '2013-11-21 00:00:00', '2013-11-09 00:00:00', NULL, NULL)
insert BorrowedHistory (ISBN, BookCopyNum, BorrowDate, ReaderPin, MustReturnDate, ReaderPin, ReturnDate, LateFine, Note)
values (1, 1, '2014-05-05 00:00:00' , 11, '2014-05-25 00:00:00', '2014-05-25 00:00:00', NULL, NULL)